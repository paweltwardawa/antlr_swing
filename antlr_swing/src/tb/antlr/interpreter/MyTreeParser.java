package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols;


public class MyTreeParser extends TreeParser {

	protected GlobalSymbols globalSymbols = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer add(Integer a, Integer b)
	{
		return a + b;
	}
	
	protected Integer sub(Integer a, Integer b)
	{
		return a - b;
	}
	
	protected Integer div(Integer a, Integer b)
	{
		return a / b;
	}
	
	protected Integer mul(Integer a, Integer b)
	{
		try
		{
			return a * b;
		}
		catch(Exception ex)
		{
			System.out.print(ex.getMessage());
			return a;
		}
		
	}
	
	protected Integer set(String name, Integer val)
	{
		globalSymbols.setSymbol(name, val);
		return val;
	}
	
	protected Integer get(String name)
	{
		return globalSymbols.getSymbol(name);
	}
}
